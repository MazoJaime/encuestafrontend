import {Component, OnInit} from '@angular/core';
import {Router, ActivatedRoute, Params} from '@angular/router';
import {User} from '../models/user';
import {UserService} from '../services/user.service';
import {DataService} from '../services/data.service';
import {AlertController} from '@ionic/angular';


@Component({
    selector: 'app-register',
    templateUrl: './register.page.html',
    styleUrls: ['./register.page.scss'],
})
export class RegisterPage implements OnInit {
    public title: string;
    public _user: User;
    public status;
    public roles;
    public datavar;
    public isadmin: boolean;

    constructor(
        private _router: Router,
        private _UserService: UserService,
        private _DataService: DataService,
        private alertCtrl: AlertController
    ) {
        this.title = 'Registro';
        this._user = new User(0, '', '', 1, '', '', '');
        this.isadmin = this._UserService.isAdmin();
        /**
         * Si no es admin mandamos a home
         */
        if (!this.isadmin) {
            this.accessdenied().then(() => {
                window.location.href = '/home';
            });

        }

    }

    ngOnInit() {


        this._DataService.getRoles().subscribe(
            response => {
                if (response.length <= 1) {
                    console.log('Error en el servidor');
                } else {

                    this.roles = response;

                }
            },
            error => {
                console.log(<any>error);
            }
        );
        console.log([this.datavar]);
    }

    onSubmit() {
        this._UserService.register(this._user).subscribe(
            response => {
                this.status = response.status;
                if (response.status !== 'success') {
                    this.status = 'error';
                }
            },
            error => {
                console.log(<any>error);
            }
        );
    }

    async accessdenied() {
        const alert = await this.alertCtrl.create({
            header: 'Acceso Denegado',
            message: 'No tienes permiso para acceder a esta zona',
            buttons: ['OK']
        });

        await alert.present();
    }
}
