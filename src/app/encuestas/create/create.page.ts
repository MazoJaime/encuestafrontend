import {Component, OnInit} from '@angular/core';
import {Encuesta} from '../../models/encuesta';
import {EncuestaService} from '../../services/encuesta.service';


@Component({
    selector: 'app-create',
    templateUrl: './create.page.html',
    styleUrls: ['./create.page.scss'],
})
export class CreatePage implements OnInit {
    public encuesta;
    public status = null;
    public mensaje = '';

    constructor(private _EncuestaService: EncuestaService) {
        this.encuesta = {
            nombre: '',
        };
    }

    ngOnInit() {
    }

    onSubmit() {
        this._EncuestaService.encuestacreate(this.encuesta).subscribe(
            response => {
                this.status = response.status;
                this.mensaje = response.msg;
            },
            error => {
                console.log(<any>error);
            }
        );
    }


}
