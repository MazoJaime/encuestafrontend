import {Component, OnInit} from '@angular/core';
import {EncuestaService} from '../../../services/encuesta.service';
import {ActivatedRoute, Params, Router} from '@angular/router';
import {Platform} from '@ionic/angular';


@Component({
    selector: 'app-encuesta',
    templateUrl: './encuesta.page.html',
    styleUrls: ['./encuesta.page.scss'],
})
export class EncuestaPage implements OnInit {
    public encuesta = {nombre: '', id: 0};
    public preguntas: Array<{ pregunta: string, id: number, valor: number }> = [];
    public datos = false;
    public encuestado = {nombre: '', rut: '', telefono: '', email: '', comentario: ''};
    public boton = 'siguiente';

    constructor(private _EncuestaService: EncuestaService,
                private _router: Router,
                private _route: ActivatedRoute,
                protected _platform: Platform
    ) {
    }

    ngOnInit() {
        this._route.params.forEach((params: Params) => {
            const id = +params['id'];
            if (id !== void (0)) {
                this._EncuestaService.getEncuesta(id).subscribe(
                    response => {
                        console.log(response);
                        this.encuesta.nombre = response[0].nombre;
                        this.encuesta.id = response[0].id;
                        for (let i = 0; i < response[0].preguntas.length; i++) {
                            this.preguntas.push({
                                    pregunta: response[0].preguntas[i].pregunta,
                                    id: response[0].preguntas[i].id,
                                    valor: 7
                                }
                            );
                        }
                    },
                    error1 => {
                        console.log(<any>error1);
                    });
            }
        });
    }

    askData() {

        if (this.datos === true) {
            this.datos = false;
            this.boton = 'Siguiente';
        } else if (this.datos === false) {
            this.datos = true;
            this.boton = 'Anterior';
        }
    }

    onSubmit() {
        this._EncuestaService.saveEncuesta(this.preguntas, this.encuesta.id, this.encuestado).subscribe(response => {
            console.log(response);
            if (response.status === 'success') {
                this._router.navigate(['/encuesta/list']);
            }
        }, error1 => {
            console.log(<any>error1);
        });
    }


}
