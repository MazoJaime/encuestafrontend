import {Component, OnInit} from '@angular/core';
import {PreguntaService} from '../../services/pregunta.service';
import {ActivatedRoute, Params, Router} from '@angular/router';
import {Platform} from '@ionic/angular';
import {EncuestaService} from '../../services/encuesta.service';

@Component({
    selector: 'app-edit',
    templateUrl: './edit.page.html',
    styleUrls: ['./edit.page.scss'],
})
export class EditPage implements OnInit {
    // id de la encuesta
    public id: number;

    // lista de todas las preguntas
    public preguntas: Array<{ texto: string, id: number, selected: boolean }> = [];

    constructor(private _PreguntaService: PreguntaService,
                private _EncuestaService: EncuestaService,
                private _router: Router,
                private _route: ActivatedRoute,
                protected _platform: Platform) {
    }

    ngOnInit(): void {
        this._route.params.forEach((params: Params) => {
            this.id = +params['id'];

            this._PreguntaService.getPreguntasEdit(this.id).subscribe(response => {

                    if (response.status === 'success') {

                        console.log(response.data);

                        for (let i = 0; i < response.data.preguntas.length; i++) {

                            const filtrado = response.data.encuesta.filter(e => e.id === response.data.preguntas[i].id);
                            let selected: boolean;
                            selected = filtrado.length > 0 ? true : false;
                            console.log(selected);
                            this.preguntas.push({
                                texto: response.data.preguntas[i].pregunta,
                                id: response.data.preguntas[i].id,
                                selected: selected
                            });
                        }

                    } else {
                        console.log([response, 'Mensaje de error']);
                    }

                }, error => {
                    console.log(<any>error);
                }
            );


        });


    }

    onSubmit() {
        this._EncuestaService.editEncuesta(this.preguntas, this.id).subscribe(response => {
            console.log(response);
        }, error => {
            console.log(<any>error);
        });

    }

}
