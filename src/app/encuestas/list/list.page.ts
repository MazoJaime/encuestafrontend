import {Component, OnInit} from '@angular/core';
import {UserService} from '../../services/user.service';
import {EncuestaService} from '../../services/encuesta.service';

@Component({
    selector: 'app-list',
    templateUrl: './list.page.html',
    styleUrls: ['./list.page.scss'],
})
export class ListPage implements OnInit {

    public identity;
    public items: Array<{ nombre: string, id: number }> = [];

    constructor(private _UserService: UserService, private _EncuestaService: EncuestaService) {
        this._UserService.checklogin();
    }

    ngOnInit() {

        this._EncuestaService.getEncuestas().subscribe(
            response => {

                for (let i = 0; i < response.length; i++) {
                    this.items.push({nombre: response[i].nombre, id: response[i].id});
                }
            },
            error => {
                console.log(<any>error);
            }
        );
    }

}
