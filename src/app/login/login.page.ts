import {Component, OnInit} from '@angular/core';
import {UserService} from '../services/user.service';
import {Events, Platform} from '@ionic/angular';
import {Router} from '@angular/router';
import {ActivatedRoute, Params} from '@angular/router';


@Component({
    selector: 'app-login',
    templateUrl: './login.page.html',
    styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
    public user;
    public token;
    public identity;
    public tittle: string;
    public status = null;
    public mensaje = '';
    public loading = false;

    constructor(private _UserService: UserService,
                public events: Events,
                private _router: Router,
                private _route: ActivatedRoute,
                private platform: Platform) {
        this.tittle = 'Login';
        this.user = {
            'email': '',
            'password': '',
            'getHash': true
        };

    }

    ngOnInit() {
        this.redirectIfIdentity();
        this.logout();
    }

    logout() {
        this._route.params.forEach((params: Params) => {
            const logout = +params['id'];
            if (logout === 1) {

                localStorage.removeItem('identity');
                localStorage.removeItem('token');
                this.identity = null;
                this.token = null;

                window.location.href = '/login';
            }
        });
    }

    redirectIfIdentity() {
        const user = this._UserService.getIdentity();
        if (user != null && user.sub) {
            window.location.href = '/home';
        }
    }

    onSubmit() {
        this.loading = true;
        this._UserService.singup(this.user).subscribe(
            response => {
                this.identity = response;
                this.status = response.status;
                this.mensaje = response.msg;
                if (this.identity.length <= 1) {
                    console.log('Error en el servidor');
                } else {
                    if (!this.identity.status) {
                        localStorage.setItem('identity', JSON.stringify(this.identity));
                        // OBTENIDO EL OBJETO SOLICITAMOS EN TOKEN gethash en false para que genere el token
                        this.user.getHash = null;
                        this._UserService.singup(this.user).subscribe(
                            response2 => {
                                this.token = response2;
                                if (this.identity.length <= 1) {
                                    console.log('Error en el servidor');
                                } else {
                                    if (!this.identity.status) {
                                        localStorage.setItem('token', this.token);
                                        this.loading = false;
                                        window.location.href = '/home';

                                    }
                                }
                            },
                            error => {
                                console.log(<any>error);
                            }
                        );
                    }
                }
                this.loading = false;
            },
            error => {
                console.log(<any>error);
            }
        );
    }

}
