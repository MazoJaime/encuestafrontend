import {Component, OnInit} from '@angular/core';
import {UserService} from '../services/user.service';


@Component({
    selector: 'app-home',
    templateUrl: 'home.page.html',
    styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {

    public identity;

    constructor(private _UserService: UserService) {

    }

    ngOnInit() {
        this._UserService.checklogin();
        this.identity = this._UserService.getIdentity();
    }
}
