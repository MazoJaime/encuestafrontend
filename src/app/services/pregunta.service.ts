import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {GLOBAL} from './global';
import {UserService} from './user.service';

@Injectable()

export class PreguntaService {

    public url;
    private headers: HttpHeaders;
    private token;

    constructor(private http: HttpClient, private _UserService: UserService) {

        this.headers = new HttpHeaders({'content-Type': 'application/x-www-form-urlencoded'});

        this.url = GLOBAL.url;

        this.token = _UserService.getToken();
    }

    preguntacreate(pregunta): Observable<any> {

        const json = JSON.stringify(pregunta);

        const params = 'pregunta=' + json + '&authorization=' + this.token;

        return this.http.post(this.url + 'pregunta/store', params, {headers: this.headers});
    }

    getPreguntas(): Observable<any> {

        const params = 'authorization=' + this.token;

        return this.http.post(this.url + 'preguntas/index', params, {headers: this.headers});
    }

    getPreguntasEdit(id): Observable<any> {

        const params = 'id=' + id + '&authorization=' + this.token;

        return this.http.post(this.url + 'encuesta/edit', params, {headers: this.headers});
    }
}
