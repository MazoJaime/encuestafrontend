import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {GLOBAL} from './global';
import {stringify} from 'querystring';
import {UserService} from './user.service';

@Injectable()

export class DataService {

    public url;
    private headers: HttpHeaders;

    constructor(private http: HttpClient, private _UserService: UserService) {
        this.headers = new HttpHeaders({'content-Type': 'application/x-www-form-urlencoded'});
        this.url = GLOBAL.url;
    }

    getRoles(): Observable<any> {
        const token = this._UserService.getToken();
        const params = 'authorization=' + token;
        return this.http.post(this.url + 'data/roles', params, {headers: this.headers});
    }


}
