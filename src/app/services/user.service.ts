import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {GLOBAL} from './global';
import {stringify} from 'querystring';

@Injectable()

export class UserService {
    public url;
    private headers: HttpHeaders;

    constructor(private http: HttpClient) {
        this.headers = new HttpHeaders({'content-Type': 'application/x-www-form-urlencoded'});
        this.headers.append('Access-Control-Allow-Origin', '*');
        this.headers.append('Access-Control-Allow-Methods', 'POST, GET, OPTIONS, PUT');
        this.url = GLOBAL.url;
    }

    /**
     * usado para validar si usuario esta logueado, de no ser asi se envia al login
     */
    checklogin() {
        const user = this.getIdentity();
        if (user === null) {
            window.location.href = '/login';
        }
    }

    getIdentity() {
        let identity = JSON.parse(localStorage.getItem('identity'));

        if (identity === void (0)) {
            identity = null;
        }
        return identity;
    }

    getToken() {
        let token = localStorage.getItem('token');
        if (token !== void (0)) {

        } else {
            token = null;
        }
        return token;
    }

    singup(user_to_login): Observable<any> {
        const json = stringify(user_to_login);
        return this.http.post(this.url + 'login', json, {headers: this.headers});
    }

    register(user_to_register): Observable<any> {
        const json = stringify(user_to_register);
        return this.http.post(this.url + 'singup', json, {headers: this.headers});
    }

    isAdmin(): boolean {
        const user = this.getIdentity();
        const admin = user.role === 1 ? user.role : false;
        return admin;
    }
}
