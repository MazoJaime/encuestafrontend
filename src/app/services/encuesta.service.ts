import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {GLOBAL} from './global';
import {UserService} from './user.service';

@Injectable()

export class EncuestaService {

    public url;
    private headers: HttpHeaders;
    private token;

    constructor(private http: HttpClient, private _UserService: UserService) {
        this.headers = new HttpHeaders({'content-Type': 'application/x-www-form-urlencoded'});
        this.url = GLOBAL.url;
        this.token = _UserService.getToken();
    }

    encuestacreate(encuesta): Observable<any> {

        const json = JSON.stringify(encuesta);

        const token = this._UserService.getToken();

        const params = 'form=' + json + '&authorization=' + token;

        return this.http.post(this.url + 'encuestas/store', params, {headers: this.headers});
    }

    getEncuestas(): Observable<any> {
        const token = this._UserService.getToken();
        const params = 'authorization=' + token;
        return this.http.post(this.url + 'data/encuestas/list', params, {headers: this.headers});
    }

    getEncuesta(id) {
        const token = this._UserService.getToken();
        const params = 'id=' + id + '&authorization=' + token;
        return this.http.post(this.url + 'encuesta/view', params, {headers: this.headers});
    }

    saveEncuesta(encuesta, id, encuestado): Observable<any> {
        const token = this._UserService.getToken();
        const json = JSON.stringify(encuesta);
        const encuestado2 = JSON.stringify(encuestado);
        const params = 'encuesta=' + json + '&id=' + id + '&cliente=' + encuestado2 + '&authorization=' + token;
        return this.http.post(this.url + 'encuesta/responder', params, {headers: this.headers});
    }

    editEncuesta(preguntas, id): Observable<any> {
        const json = JSON.stringify(preguntas);
        const params = 'encuesta=' + json + '&id=' + '&authorization=' + this.token;
        // todo: general backend que hagan un sync de las respuesta
        return this.http.post(this.url + 'encuesta/edit/' + id, params, {headers: this.headers});

    }
}
