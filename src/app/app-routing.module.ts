import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';

const routes: Routes = [
    {
        path: '',
        redirectTo: 'login',
        pathMatch: 'full'
    },
    {
        path: 'home',
        loadChildren: './home/home.module#HomePageModule'
    },
    {
        path: 'list',
        loadChildren: './list/list.module#ListPageModule'
    },
    {
        path: 'login',
        loadChildren: './login/login.module#LoginPageModule'
    },
    {
        path: 'new-user',
        loadChildren: './register/register.module#RegisterPageModule'
    },
    {
        path: 'login/:id',
        loadChildren: './login/login.module#LoginPageModule'
    },
    {path: 'encuesta/create', loadChildren: './encuestas/create/create.module#CreatePageModule'},
    {path: 'encuesta/list', loadChildren: './encuestas/list/list.module#ListPageModule'},
    {path: 'encuesta/:id', loadChildren: './encuestas/ver/encuesta/encuesta.module#EncuestaPageModule'},
    {path: 'encuesta/edit/:id', loadChildren: './encuestas/edit/edit.module#EditPageModule'},
    {path: 'pregunta/create', loadChildren: './preguntas/create/create.module#CreatePageModule'},
    {path: 'pregunta/list', loadChildren: './preguntas/list/list.module#ListPageModule'},

    {path: '**', redirectTo: 'home'},


];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule {
}
