import {Component} from '@angular/core';

import {Platform} from '@ionic/angular';
import {SplashScreen} from '@ionic-native/splash-screen/ngx';
import {StatusBar} from '@ionic-native/status-bar/ngx';
import {UserService} from './services/user.service';
import {DataService} from './services/data.service';

@Component({
    selector: 'app-root',
    templateUrl: 'app.component.html'
})
export class AppComponent {

    public appPages;
    public identity;
    public token;
    public roles;

    constructor(
        private platform: Platform,
        private splashScreen: SplashScreen,
        private statusBar: StatusBar,
        private _UserService: UserService,
        private _DataService: DataService
    ) {
        this.initializeApp();
        this._DataService.getRoles().subscribe(
            response => {
                if (response.length <= 1) {
                    console.log('Error en el servidor');
                } else {
                    this.roles = response;
                }
            },
            error => {
                console.log(<any>error);
            }
        );
    }

    initializeApp() {
        this.platform.ready().then(() => {


            if (this.platform.is('android')) {
                this.statusBar.styleBlackOpaque();
                this.splashScreen.hide();
            } else {
                /*console.log([this._UserService.getIdentity(), this._UserService.getToken()]);*/
            }
            this.identity = this._UserService.getIdentity();
            this.token = this._UserService.getToken();
            if (this.identity && this.identity.role) {
                switch (this.identity.role) {
                    case 1:
                        this.appPages = [
                            {
                                title: 'Inicio',
                                url: '/home',
                                icon: 'home'
                            },
                            {
                                title: 'Nueva Encuesta',
                                url: '/encuesta/create',
                                icon: 'add-circle'
                            },
                            {
                                title: 'Nueva Pegunta',
                                url: '/pregunta/create',
                                icon: 'add'
                            },
                            {
                                title: 'Encuestas',
                                url: '/encuesta/list',
                                icon: 'list'
                            },
                            {
                                title: 'Preguntas',
                                url: '/pregunta/list',
                                icon: 'list'
                            },
                            {
                                title: 'Nuevo Usuario',
                                url: '/new-user',
                                icon: 'person-add'
                            }, {
                                title: 'Logout',
                                url: '/login/1',
                                icon: 'exit'
                            }

                        ];
                        break;
                    case 2:
                        this.appPages = [
                            {
                                title: 'Encuesta',
                                url: '/home',
                                icon: 'home'
                            },

                            {
                                title: 'Logout',
                                url: '/login/1',
                                icon: 'cog'
                            }
                        ];
                        break;
                }

            }

        });

    }
}
