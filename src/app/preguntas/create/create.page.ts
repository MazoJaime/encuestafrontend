import {Component, OnInit} from '@angular/core';
import {PreguntaService} from '../../services/pregunta.service';

@Component({
    selector: 'pregunta-create',
    templateUrl: './create.page.html',
    styleUrls: ['./create.page.scss'],
})
export class CreatePage implements OnInit {
    public pregunta;
    public status = null;
    public mensaje = '';

    constructor(private _PreguntaService: PreguntaService) {
        this.pregunta = {
            texto: '',
        };
    }

    ngOnInit() {
    }

    onSubmit() {
        this._PreguntaService.preguntacreate(this.pregunta).subscribe(
            response => {
                console.log(response);
                this.status = response.status === 'success' ? 'success' : 'danger';
                this.mensaje = response.msg;
            }, error => {
                console.log(<any>error);
            });
    }
}
