import {Component, OnInit} from '@angular/core';
import {PreguntaService} from '../../services/pregunta.service';

@Component({
    selector: 'app-list-preguntas',
    templateUrl: './list.page.html',
    styleUrls: ['./list.page.scss'],
})
export class ListPage implements OnInit {

    public preguntas: Array<{ id: number, texto: string }> = [];

    constructor(private _PreguntaService: PreguntaService) {
    }

    ngOnInit() {
        this._PreguntaService.getPreguntas().subscribe(response => {
                console.log(response);

                if (response.status === 'success') {

                    for (let i = 0; i < response.msg.length; i++) {
                        this.preguntas.push({texto: response.msg[i].pregunta, id: response.msg[i].id});
                    }

                } else {
                    console.log(response);
                }

            }, error => {
                console.log(<any>error);
            }
        );
    }

}
